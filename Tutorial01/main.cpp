#include <iostream>
#include <ctime>
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"

using std::cin;
using std::cout;
using std::endl;

void printMean(std::vector<cv::Scalar> &input ){
    int count = 0;
    for(std::vector<cv::Scalar>::iterator current = input.begin(); current != input.end(); ++current){
        printf("%02d: B:%2f G:%2f R:%2f \n", count++, (*current)[0], (*current)[1], (*current)[2]);
    }
}

std::vector<cv::Scalar> calculateMeans(std::vector<cv::Mat> input){
    std::vector<cv::Scalar> result;
    std::for_each(input.begin(), input.end(), [&result](cv::Mat &element){result.push_back(cv::mean(element));});
    return result;
}

cv::Scalar calculateTargetMean(std::vector<cv::Scalar> &input){
    double temp[4] = {0,0,0,0};
    cv::Scalar result;

    for(std::vector<cv::Scalar>::iterator it = input.begin(); it != input.end(); it++){
        for(int i = 0; i < 4; i++)
            temp[i] += (*it)[i];
    }

    for(int i = 0; i < 4; i++)
        result[i] = temp[i] / (double)input.size();

    return result;
}

void equalizeBrightness(std::vector<cv::Mat> &input){
    //get channel value means for each channel
    std::vector<cv::Scalar> imageMeans = calculateMeans(input);

    //calculate the mean between all channels -> target mean
    cv::Scalar targetMean = calculateTargetMean(imageMeans);

    //adjust images according to target mean
    for(int i = 0; i < input.size(); i++){
        input[i] = input[i] * (imageMeans[i] / targetMean);
    }

}

cv::Mat make3dImage(std::vector<cv::Mat> &input){
    if(input[0].size != input[1].size) std::cout << "supplied images do not have same dimensions / image parameters" << std::endl;
    cv::Mat channels_left[3];
    cv::split(input[0],channels_left);
    cv::Mat channels_right[3];
    cv::split(input[1],channels_right);
    cv::Mat channels_result[3] = {channels_right[0], channels_right[1], channels_left[2]};
    cv::Mat result;
    cv::merge(channels_result, 3, result);

    return result;
}

cv::Mat make3dImage_ugly(std::vector<cv::Mat> &input){
    cv::Mat result = cv::Mat(input[0].size(), CV_8UC3);
    for(int col = 0; col < result.cols; col++)
        for(int row = 0; row < result.rows; row++)
            result.at<cv::Vec3b>(row, col) = cv::Vec3b(
                    input[1].at<cv::Vec3b>(row,col)[0],
                    input[1].at<cv::Vec3b>(row,col)[1],
                    input[0].at<cv::Vec3b>(row,col)[2]
            );

    return result;
}

int main(int argc, char** argv) {

    std::vector<cv::Mat> images;

    images.push_back(cv::imread("im_left.png"));
    images.push_back(cv::imread("im_right.png"));

    std::vector<cv::Scalar> meanValues = calculateMeans(images);
    printMean(meanValues);
    equalizeBrightness(images);

    std::clock_t begin = clock();
    cv::Mat result = make3dImage(images);
    double time_cv = double(clock()-begin) / CLOCKS_PER_SEC;

    std::cout << "OpenCV took " << time_cv <<" sec." << std::endl;

    begin = clock();
    result = make3dImage_ugly(images);
    double time_ugly = double(clock()-begin)/CLOCKS_PER_SEC;

    std::cout << "Ugly took " << time_ugly <<" sec." << std::endl;

    cv::imshow("Display Window", result);

    meanValues = calculateMeans(images);
    printMean(meanValues);

    fflush(stdout);
    cv::waitKey(0);
    return 0;
}
